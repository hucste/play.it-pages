====== Dungeon Keeper II ======

===== Description =====

[[https://www.dotslashplay.it/images/games/dungeon-keeper-2/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/dungeon-keeper-2/thumbnail.jpg?nocache }}]]

**Dungeon Keeper II**: Discover your evil side as you build your own underground kingdom.\\
Carve out a living, breathing world and attract a host of devilish creatures to swell the ranks of your dark hoards.\\
Form an alliance with the Horned Reaper and expand your empire in your quest to reach the daylight and invade the realms above.

  * category: strategy, action, fantasy
  * release date: 1999

===== Informations =====

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-dungeon-keeper-2.sh|play-dungeon-keeper-2.sh]] (updated on 30/12/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * target:
    * [[https://www.gog.com/game/dungeon_keeper_2|setup_dungeon_keeper2_2.0.0.32.exe]] (MD5: 92d04f84dd870d9624cd18449d3622a5)
  * dependencies:
    * Arch Linux:
      * icoutils
      * innoextract
    * Debian:
      * fakeroot
      * icoutils
      * innoextract

<note>The game installed via these scripts will use [[https://www.winehq.org/|WINE]].</note>

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
$ yaourt icoutils innoextract
</code>
    - Debian:<code>
# apt-get install fakeroot icoutils innoextract
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
libplayit2.sh
play-dungeon-keeper-2.sh
setup_dungeon_keeper2_2.0.0.32.exe
</code>
  - Start the building process from this directory:<code>
$ sh ./play-dungeon-keeper-2.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
