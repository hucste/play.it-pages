====== Blackwell 2: Blackwell Unbound ======

===== Description =====

**Blackwell Unbound**: 1973. The sound of a lone, ethereal saxophone drifts over the Roosevelt Island promenade, while a series of accidents plague a midtown construction site. The citizens of Manhattan take no notice of these events, let alone think they are connected.\\
Embittered medium Lauren Blackwell and her spirit guide Joey Mallone are the only ones who believe that there is anything strange going on, and they are the only ones who can stop an enigmatic killer from striking again…

  * age: 12y+
  * category:  adventure, esoterism, Point’n Click
  * official url: [[http://www.wadjeteyegames.com/games/blackwell-unbound/|]]
  * release date: 2007

===== Informations =====

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-blackwell-2-blackwell-unbound.sh|play-blackwell-2-blackwell-unbound.sh]] (updated on 14/01/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * target:
    * [[https://www.gog.com/game/blackwell_bundle|gog_blackwell_unbound_2.0.0.2.sh]] (MD5: e694b6638f49535224ed474d3c8ce128)
  * dependencies:
    * Arch Linux:
      * libarchive
    * Debian:
      * bsdtar
      * fakeroot

[[https://www.dotslashplay.it/images/games/blackwell-2-blackwell-unbound/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/blackwell-2-blackwell-unbound/thumbnail.jpg?nocache }}]]

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
# pacman -S libarchive
</code>
    - Debian:<code>
# apt-get install bsdtar fakeroot
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
gog_blackwell_unbound_2.0.0.2.sh
libplayit2.sh
play-blackwell-2-blackwell-unbound.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-blackwell-2-blackwell-unbound.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
