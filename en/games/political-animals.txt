====== Political Animals ======

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-political-animals.sh|play-political-animals.sh]] (updated on 10/12/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * target:
    * [[https://www.gog.com/game/political_animals|gog_political_animals_2.1.0.4.sh]] (MD5: efaab47c43abc738d1dfd358c0894d4d)
  * dependencies:
    * Arch Linux:
      * libarchive
    * Debian:
      * bsdtar
      * fakeroot

[[https://www.dotslashplay.it/images/games/political-animals/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/political-animals/thumbnail.jpg?nocache }}]]

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
# pacman -S libarchive
</code>
    - Debian:<code>
# apt-get install bsdtar fakeroot
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
libplayit2.sh
play-political-animals.sh
gog_political_animals_2.1.0.4.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-political-animals.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
