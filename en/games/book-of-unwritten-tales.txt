====== The Book of Unwritten Tales ======

===== Description =====

[[https://www.dotslashplay.it/images/games/the-book-of-unwritten-tales/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/the-book-of-unwritten-tales/thumbnail.jpg?nocache }}]]

**The Book of Unwritten Tales**: a gremlin archaeologist is the only one who knows the secret location of a very special artifact.\\
The archaeologist was kidnapped by an evil sorcerer to reveal the location of the object.\\
Three heroes decide to leave to save the gremlin. He's an elf, a gnome and a human.\\
What they don't know yet is that in carrying out this quest, they risk saving the world from destruction.

  * age: 12y+
  * category: adventure
  * release date: 2010

===== Informations =====

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-the-book-of-unwritten-tales.sh|play-the-book-of-unwritten-tales.sh]] (updated on 13/02/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * targets:
    * [[https://www.gog.com/game/the_book_of_unwritten_tales|setup_book_of_unwritten_tales_2.0.0.4.exe]] (MD5: 984e8f16cc04a2a27aea8b0d7ada1c1e)
    * [[https://www.gog.com/game/the_book_of_unwritten_tales|setup_book_of_unwritten_tales_2.0.0.4-1.bin]] (MD5: 4ea0eccb7ca2f77c301e79412ff1e214)
    * [[https://www.gog.com/game/the_book_of_unwritten_tales|setup_book_of_unwritten_tales_2.0.0.4-2.bin]] (MD5: 95e52d38b6c1548ac311284c539a4c52)
    * [[https://www.gog.com/game/the_book_of_unwritten_tales|setup_book_of_unwritten_tales_2.0.0.4-3.bin]] (MD5: 7290d78ecbec866e46401e4c9d3549cf)
  * dependencies:
    * Arch Linux:
      * icoutils
      * innoextract
    * Debian:
      * fakeroot
      * icoutils
      * innoextract

<note>The game installed via these scripts will use [[https://www.winehq.org/|WINE]].</note>

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
$ yaourt icoutils innoextract
</code>
    - Debian:<code>
# apt-get install fakeroot icoutils innoextract
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
libplayit2.sh
play-the-book-of-unwritten-tales.sh
setup_book_of_unwritten_tales_2.0.0.4.exe
setup_book_of_unwritten_tales_2.0.0.4-1.bin
setup_book_of_unwritten_tales_2.0.0.4-2.bin
setup_book_of_unwritten_tales_2.0.0.4-3.bin
</code>
  - Start the building process from this directory:<code>
$ sh ./play-the-book-of-unwritten-tales.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
