====== Banished ======

===== Description =====

[[https://www.dotslashplay.it/images/games/banished/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/banished/thumbnail.jpg?nocache }}]]

**Banished**: create a city and manage it according to the mechanics of games (people’s happiness, management of primary needs such as food and heating, input of raw materials, etc.).

  * category: management, city builder
  * official url: [[http://www.shiningrocksoftware.com/game/|]]
  * release date: 2014

===== Informations =====

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-banished.sh|play-banished.sh]] (updated on 13/01/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * targets:
    * 32-bit version:
      * [[https://www.gog.com/game/banished|setup_banished_32_1.0.7_(14938).exe]] (MD5: 43042701a692f186d467b97e966fb846)
    * 64-bit version:
      * [[https://www.gog.com/game/banished|setup_banished_64_1.0.7_(14938).exe]] (MD5: 463b2720c5c88c28f24de9176b8b1ec4)
  * dependencies:
    * Arch Linux:
      * icoutils
      * innoextract
    * Debian:
      * fakeroot
      * icoutils
      * innoextract

<note>The game installed via these scripts will use [[https://www.winehq.org/|WINE]].</note>

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
$ yaourt icoutils innoextract
</code>
    - Debian:<code>
# apt-get install fakeroot icoutils innoextract
</code>
  - Put in a same directory the scripts and archive:
    - 32-bit version:<code>
$ ls
</code><code>
libplayit2.sh
play-banished.sh
setup_banished_32_1.0.7_(14938).exe
</code>
    - 64-bit version:<code>
$ ls
</code><code>
libplayit2.sh
play-banished.sh
setup_banished_64_1.0.7_(14938).exe
</code>
  - Start the building process from this directory:<code>
$ sh ./play-banished.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
