====== Gnomoria ======

===== Description =====

[[https://www.dotslashplay.it/images/games/gnomoria/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/gnomoria/thumbnail.jpg?nocache }}]]

**Gnomoria** is a sandbox village management game where you help lead a small group of gnomes, who have set out on their own, to thrive into a bustling kingdom!\\
Anything you see can be broken down and rebuilt elsewhere. Craft items, build structures, set traps, and dig deep underground in search of precious resources to help your gnomes survive the harsh lands.\\
Build your kingdom and stockpile wealth to attract wandering gnomads to your cause, but be wary of also attracting enemies!

  * category: Strategy, City-Builder
  * release date: 2016

===== Informations =====

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-gnomoria.sh|play-gnomoria.sh]] (updated on 24/08/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * target:
    * [[https://www.gog.com/game/gnomoria|gog_gnomoria_2.0.0.1.sh]] (MD5: 3d0a9ed4fb45ff133b5a7410a2114455)
  * dependencies:
    * Arch Linux:
      * libarchive
    * Debian:
      * bsdtar
      * fakeroot

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
# pacman -S libarchive
</code>
    - Debian:<code>
# apt-get install bsdtar fakeroot
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
gog_gnomoria_2.0.0.1.sh
libplayit2.sh
play-gnomoria.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-gnomoria.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>

==== Links ====

  * [[http://gnomoria.com/|official website]]
