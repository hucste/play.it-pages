====== else Heart.Break() ======

===== Description =====

[[https://www.dotslashplay.it/images/games/else-heart-break/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/else-heart-break/thumbnail.jpg?nocache }}]]

**else Heart.Break**: Sebastian has just landed his first job in the distant city of Dorisburg. He moves there to start his adult life and figure out who he really wants to be. Among a strange collection of people, hackers and activists he finds some true friends – perhaps even love. But can they stop the terrible deeds of the people ruling the city? And who will get their heart broken in the end?

  * category: arcade, adventure
  * official url: [[http://elseheartbreak.com/|]]
  * release date: 2015

===== Informations =====

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-else-heart-break.sh|play-else-heart-break.sh]] (updated on 24/08/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * target:
    * [[https://www.humblebundle.com/store/else-heartbreak|ElseHeartbreakLinux.tgz]] (MD5: 7030450cadac6234676967ae41f2a732)
  * dependencies:
    * Debian:
      * fakeroot

==== Usage ====

  - Install the scripts dependencies:
    - Debian:<code>
# apt-get install fakeroot
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
ElseHeartbreakLinux.tgz
libplayit2.sh
play-else-heart-break.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-else-heart-break.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>

==== Links ====

  * [[http://elseheartbreak.com/|official website]]
