====== Gobliins 2: The Prince Buffoon ======

===== Description =====

[[https://www.dotslashplay.it/images/games/gobliins-2/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/gobliins-2/thumbnail.jpg?nocache }}]]

**Gobliins 2**: King Angoulafre’s son is missing. The magician of the court, Modemus, decides to send two elves to the prince's rescue.\\
He magically transports them to a faraway country so that one of his colleagues, the magician Tazaar, can help them.\\
They learn then that the person in charge is a devil who has risen from nothing, the terrible Ammoniak…

  * category: Adventure
  * release date: 1992

===== Informations =====

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-gobliins-2.sh|play-gobliins-2.sh]] (updated on 10/03/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * target:
    * [[https://www.gog.com/game/gobliiins_pack|setup_gobliiins2_2.1.0.63.exe]] (MD5: 0baf2ce55d00fce9af4c98848e88d7dc)
  * dependencies:
    * Arch Linux:
      * icoutils
      * innoextract
    * Debian:
      * fakeroot
      * icoutils
      * innoextract

<note>The game installed via these scripts will run through [[https://www.scummvm.org/|ScummVM]].</note>

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
$ yaourt -S icoutils innoextract
</code>
    - Debian:<code>
# apt-get install fakeroot icoutils innoextract
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
libplayit2.sh
play-gobliins-2.sh
setup_gobliiins2_2.1.0.63.exe
</code>
  - Start the building process from this directory:<code>
$ sh ./play-gobliins-2.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
$ yaourt -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
