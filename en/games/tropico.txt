====== Tropico ======

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-tropico.sh|play-tropico.sh]] (updated on 2/09/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * targets:
    * English version:
      * [[https://www.gog.com/game/tropico_reloaded|setup_tropico_2.1.0.14.exe]] (MD5: 1bd761bc4a40a42a9caeb41c70d46465)
    * French version:
      * [[https://www.gog.com/game/tropico_reloaded|setup_tropico_french_2.1.0.14.exe]] (MD5: aad4ea5a6fe2b2c2f347cfa7aae058b3)
  * dependencies:
    * Arch Linux:
      * icoutils
      * innoextract
    * Debian:
      * fakeroot
      * icoutils
      * innoextract

[[https://www.dotslashplay.it/images/games/tropico/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/tropico/thumbnail.jpg?nocache }}]]

<note>The game installed via these scripts will use [[https://www.winehq.org/|WINE]].</note>

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
$ yaourt icoutils innoextract
</code>
    - Debian:<code>
# apt-get install fakeroot icoutils innoextract
</code>
  - Put in a same directory the scripts and archive:
    - English version:<code>
$ ls
</code><code>
libplayit2.sh
play-tropico.sh
setup_tropico_2.1.0.14.exe
</code>
    - French version:<code>
$ ls
</code><code>
libplayit2.sh
play-tropico.sh
setup_tropico_french_2.1.0.14.exe
</code>
  - Start the building process from this directory:<code>
$ sh ./play-tropico.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
