====== Machinarium ======

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-machinarium.sh|play-machinarium.sh]] (updated on 24/01/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * target:
    * [[https://www.gog.com/game/machinarium_collectors_edition|gog_machinarium_2.0.0.2.sh]] (MD5: 4a66896935fbf29f4816e615748bb679)
  * dependencies:
    * Arch Linux:
      * libarchive
    * Debian:
      * bsdtar
      * fakeroot

[[https://www.dotslashplay.it/images/games/machinarium/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/machinarium/thumbnail.jpg?nocache }}]]

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
# pacman -S libarchive
</code>
    - Debian:<code>
# apt-get install bsdtar fakeroot
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
gog_machinarium_2.0.0.2.sh
libplayit2.sh
play-machinarium.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-machinarium.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
