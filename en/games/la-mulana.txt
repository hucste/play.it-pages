====== La•Mulana ======
//version sold on Humble Bundle//

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-la-mulana.sh|play-la-mulana.sh]] (updated on 31/10/2016)
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-anything.sh|play-anything.sh]] (updated on 18/03/2018)
  * target:
    * LaMulanaSetup-2016-01-06.sh (MD5: 24c4e7df8853af24172ede02b6016099)
  * dependencies:
    * fakeroot
    * unzip

[[https://www.dotslashplay.it/images/games/la-mulana/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/la-mulana/thumbnail.jpg?nocache }}]]

==== Usage ====

  - Install the scripts dependencies:<code>
# apt-get install fakeroot unzip
</code>
  - Put in a same directory the scripts and installer:<code>
$ ls
</code><code>
LaMulanaSetup-2016-01-06.sh
play-anything.sh
play-la-mulana.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-la-mulana.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>

==== Advanced usage ====

This script accept several options to set its behaviour. Follow the links to more details on how to use them.

  * [[:en:common:advanced-options#checksum|checksum]]
  * [[:en:common:advanced-options#compression|compression]]
  * [[:en:common:advanced-options#help|help]]
  * [[:en:common:advanced-options#prefix|prefix]]

==== Links ====

  * [[https://en.wikipedia.org/wiki/La-Mulana|Wikipedia article]]
  * [[http://la-mulana.com/en/|official website]]
