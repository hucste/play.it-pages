====== Volgarr the Viking ======
//version sold on GOG//

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-volgarr-the-viking_gog-2.1.0.3.sh|play-volgarr-the-viking_gog-2.1.0.3.sh]] (updated on 15/04/2016)
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-anything.sh|play-anything.sh]] (updated on 18/03/2018)
  * target:
    * gog_volgarr_the_viking_2.1.0.3.sh (MD5: 8593287f13c3104aa45b9c91264b4260)
  * dependencies:
    * fakeroot
    * unzip

[[https://www.dotslashplay.it/images/games/volgarr-the-viking/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/volgarr-the-viking/thumbnail.jpg?nocache }}]]

==== Usage ====

  - Install the scripts dependencies:<code>
# apt-get install fakeroot unzip
</code>
  - Put in a same directory scripts and installer:<code>
$ ls
</code><code>
gog_volgarr_the_viking_2.1.0.3.sh
play-anything.sh
play-volgarr-the-viking_gog-2.1.0.3.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-volgarr-the-viking_gog-2.1.0.3.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>

==== Advanced usage ====

This script accept several options to set its behaviour. Follow the links to more detai$ ls on how to use them.

  * [[:en:common:advanced-options#checksum|checksum]]
  * [[:en:common:advanced-options#compression|compression]]
  * [[:en:common:advanced-options#help|help]]
  * [[:en:common:advanced-options#prefix|prefix]]

==== Links ====

  * [[https://en.wikipedia.org/wiki/Volgarr_the_Viking|Wikipedia article]]
