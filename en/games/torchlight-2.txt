====== Torchlight II ======

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-torchlight-2.sh|play-torchlight-2.sh]] (updated on 8/12/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * targets:
    * GOG:
      * [[https://www.gog.com/game/torchlight_ii|gog_torchlight_2_2.0.0.2.sh]] (MD5: e107f6d4c6d4cecea37ade420a8d4892)
    * Humble Bundle:
      * [[https://www.humblebundle.com/store/torchlight-ii|Torchlight2-linux-2015-04-01.sh]] (MD5: 730a5d08c8f1cd4a65afbc0ca631d85c)
  * dependencies:
    * Arch Linux:
      * libarchive
    * Debian:
      * bsdtar
      * fakeroot

[[https://www.dotslashplay.it/images/games/torchlight-2/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/torchlight-2/thumbnail.jpg?nocache }}]]

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
# pacman -S libarchive
</code>
    - Debian:<code>
# apt-get install bsdtar fakeroot
</code>
  - Put in a same directory the scripts and archive:
    - GOG:<code>
$ ls
</code><code>
gog_torchlight_2_2.0.0.2.sh
libplayit2.sh
play-torchlight-2.sh
</code>
    - Humble Bundle:<code>
$ ls
</code><code>
Torchlight2-linux-2015-04-01.sh
libplayit2.sh
play-torchlight-2.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-torchlight-2.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
