====== The Elder Scrolls III: Morrowind ======

===== Description =====

[[https://www.dotslashplay.it/images/games/morrowind/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/morrowind/thumbnail.jpg?nocache }}]]

**The Elder Scrolls III: Morrowind**: The Lost Prophecies speak of the Incarnate, a reincarnation of the Dunmer hero Nerevar, arriving in Morrowind to rid the land of a dark curse. To fulfill this prophecy, the Emperor sends an unknown and uncertain imperial courier to the island of Vvardenfell. Through a series of dangerous and magical quests, this unknown courier is transformed into one of the Emprire’s most enduring heroes.

  * age: 18y+
  * category: RPG, action, aventure
  * official url: [[https://elderscrolls.bethesda.net/en/morrowind|]]
  * release date: 2002

===== Informations =====

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-morrowind.sh|play-morrowind.sh]] (updated on 14/01/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * targets:
    * English version:
      * [[https://www.gog.com/game/the_elder_scrolls_iii_morrowind_goty_edition|setup_tes_morrowind_goty_2.0.0.7.exe]] (MD5: 3a027504a0e4599f8c6b5b5bcc87a5c6)
    * French version:
      * [[https://www.gog.com/game/the_elder_scrolls_iii_morrowind_goty_edition|setup_tes_morrowind_goty_french_2.0.0.7.exe]] (MD5: 2aee024e622786b2cb5454ff074faf9b)
  * dependencies:
    * Arch Linux:
      * icoutils
      * innoextract
    * Debian:
      * fakeroot
      * icoutils
      * innoextract

<note>The game installed via these scripts will use [[https://www.winehq.org/|WINE]].</note>

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
$ yaourt icoutils innoextract
</code>
    - Debian:<code>
# apt-get install fakeroot icoutils innoextract
</code>
  - Put in a same directory the scripts and archive:
    - English version:<code>
$ ls
</code><code>
libplayit2.sh
play-morrowind.sh
setup_tes_morrowind_goty_2.0.0.7.exe
</code>
    - French version:<code>
$ ls
</code><code>
libplayit2.sh
play-morrowind.sh
setup_tes_morrowind_goty_french_2.0.0.7.exe
</code>
  - Start the building process from this directory:<code>
$ sh ./play-morrowind.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
