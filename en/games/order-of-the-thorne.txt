====== Order of the Thorne: The King’s Challenge ======

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-order-of-the-thorne.sh|play-order-of-the-thorne.sh]] (updated on 3/03/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * target:
    * [[https://www.gog.com/game/order_of_the_thorne_the_kings_challenge|gog_order_of_the_thorne_the_king_s_challenge_2.0.0.1.sh]] (MD5: 16dde031dcfb730be1d94bde77306b0d)
  * dependencies:
    * Arch Linux:
      * libarchive
    * Debian:
      * bsdtar
      * fakeroot

[[https://www.dotslashplay.it/images/games/order-of-the-thorne/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/order-of-the-thorne/thumbnail.jpg?nocache }}]]

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
# pacman -S libarchive
</code>
    - Debian:<code>
# apt-get install bsdtar fakeroot
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
gog_order_of_the_thorne_the_king_s_challenge_2.0.0.1.sh
libplayit2.sh
play-order-of-the-thorne.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-order-of-the-thorne.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
