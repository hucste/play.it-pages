====== Day of the Tentacle ======

===== Description =====

[[https://www.dotslashplay.it/images/games/day-of-the-tentacle-remastered/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/day-of-the-tentacle-remastered/thumbnail.jpg?nocache }}]]

**Day of the Tentacle** is an enigmatic adventure that defies understanding and time: three unlikely friends cooperate to prevent an evil, mutant purple tentacle from conquering the world! But it’s already too late to stop him…\\
To the time machine!!

  * age: 13y+
  * category: adventure, Point’n Click
  * official url: [[http://dott.doublefine.com/|]]
  * release date: 2016

===== Informations =====

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-day-of-the-tentacle-remastered.sh|play-day-of-the-tentacle-remastered.sh]] (updated on 28/12/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * target:
    * [[https://www.gog.com/game/day_of_the_tentacle_remastered|gog_day_of_the_tentacle_remastered_2.1.0.2.sh]] (MD5: 612c59c5cbdbf4d73322b46527a2d502)
  * dependencies:
    * Arch Linux:
      * libarchive
    * Debian:
      * bsdtar
      * fakeroot

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
# pacman -S libarchive
</code>
    - Debian:<code>
# apt-get install bsdtar fakeroot
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
gog_day_of_the_tentacle_remastered_2.1.0.2.sh
libplayit2.sh
play-day-of-the-tentacle-remastered.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-day-of-the-tentacle-remastered.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>

==== Links ====

  * [[https://en.wikipedia.org/wiki/Day_of_the_Tentacle|Wikipedia article]]
