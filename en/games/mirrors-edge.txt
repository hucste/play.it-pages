====== Mirror’s Edge ======

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-mirrors-edge.sh|play-mirrors-edge.sh]] (updated on 3/02/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * targets:
    * [[https://www.gog.com/game/mirrors_edge|setup_mirrors_edge_2.0.0.3.exe]] (MD5: 89381d67169f5c6f8f300e172a64f99c)
    * [[https://www.gog.com/game/mirrors_edge|setup_mirrors_edge_2.0.0.3-1.bin]] (MD5: 406b99108e1edd17fc60435d1f2c27f9)
    * [[https://www.gog.com/game/mirrors_edge|setup_mirrors_edge_2.0.0.3-2.bin]] (MD5: 18f2bd62201904c8e98a4b805a90ab2d)
  * dependencies:
    * Arch Linux:
      * icoutils
      * unarchiver
    * Debian:
      * fakeroot
      * icoutils
      * unar

[[https://www.dotslashplay.it/images/games/mirrors-edge/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/mirrors-edge/thumbnail.jpg?nocache }}]]

<note>The game installed via these scripts will use [[https://www.winehq.org/|WINE]].</note>

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
$ yaourt icoutils unarchiver
</code>
    - Debian:<code>
# apt-get install fakeroot icoutils unar
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
libplayit2.sh
play-mirrors-edge.sh
setup_mirrors_edge_2.0.0.3.exe
setup_mirrors_edge_2.0.0.3-1.bin
setup_mirrors_edge_2.0.0.3-2.bin
</code>
  - Start the building process from this directory:<code>
$ sh ./play-mirrors-edge.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
