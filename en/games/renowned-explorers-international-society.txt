====== Renowned Explorers: International Society ======

=== Renowned Explorers: International Society ===

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-renowned-explorers-international-society.sh|play-renowned-explorers-international-society.sh]] (updated on 28/12/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * target:
    * [[https://www.gog.com/game/renowned_explorers|renowned_explorers_international_society_en_466_15616.sh]] (MD5: fbad4b4d361a0e7d29b9781e3c5a5e85)
  * dependencies:
    * Arch Linux:
      * libarchive
    * Debian:
      * bsdtar
      * fakeroot

=== Renowned Explorers: More To Explore ===

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-renowned-explorers-more-to-explore.sh|play-renowned-explorers-more-to-explore.sh]] (updated on 28/12/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * target:
    * [[https://www.gog.com/game/renowned_explorers_more_to_explore|renowned_explorers_more_to_explore_dlc_en_466_15616.sh]] (MD5: c99ca440cb312b90052939db49aeef03)
  * dependencies:
    * Arch Linux:
      * libarchive
    * Debian:
      * bsdtar
      * fakeroot

=== Renowned Explorers: The Emperor’s Challenge ===

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-renowned-explorers-the-emperors-challenge.sh|play-renowned-explorers-the-emperors-challenge.sh]] (updated on 18/03/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * target:
    * [[https://www.gog.com/game/renowned_explorers_the_emperors_challenge|renowned_explorers_the_emperor_s_challenge_dlc_en_466_15616.sh]] (MD5: 12baa49b557c92e2f5eae7ff99623d34)
  * dependencies:
    * Arch Linux:
      * libarchive
    * Debian:
      * bsdtar
      * fakeroot

[[https://www.dotslashplay.it/images/games/renowned-explorers-international-society/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/renowned-explorers-international-society/thumbnail.jpg?nocache }}]]

==== Usage (Renowned Explorers: International Society) ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
# pacman -S libarchive
</code>
    - Debian:<code>
# apt-get install bsdtar fakeroot
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
libplayit2.sh
play-renowned-explorers-international-society.sh
renowned_explorers_international_society_en_466_15616.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-renowned-explorers-international-society.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>

==== Usage (Renowned Explorers: More To Explore) ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
# pacman -S libarchive
</code>
    - Debian:<code>
# apt-get install bsdtar fakeroot
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
libplayit2.sh
play-renowned-explorers-more-to-explore.sh
renowned_explorers_more_to_explore_dlc_en_466_15616.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-renowned-explorers-more-to-explore.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>

==== Usage (Renowned Explorers: The Emperor’s Challenge) ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
# pacman -S libarchive
</code>
    - Debian:<code>
# apt-get install bsdtar fakeroot
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
libplayit2.sh
play-renowned-explorers-the-emperors-challenge.sh
renowned_explorers_the_emperor_s_challenge_dlc_en_466_15616.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-renowned-explorers-the-emperors-challenge.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
