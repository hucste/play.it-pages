====== Hand Of Fate ======

===== Description =====

**Hand Of Fate**: Beyond the thirteen gates at the end of the world, the game of life and death is played.\\
Draw your cards, play your hand, and discover your fate

  * category: RPG, Action
  * official url: [[http://www.defiantdev.com/hof1.html|]]
  * release date: 2015

===== Informations =====

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-hand-of-fate.sh|play-hand-of-fate.sh]] (updated on 31/03/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * target:
    * [[https://www.gog.com/game/hand_of_fate|gog_hand_of_fate_2.12.0.16.sh]] (MD5: 54c61dce76b1281b4161d53d096d6ffe)
  * dependencies:
    * Arch Linux:
      * libarchive
    * Debian:
      * bsdtar
      * fakeroot

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
# pacman -S libarchive
</code>
    - Debian:<code>
# apt-get install bsdtar fakeroot
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
gog_hand_of_fate_2.12.0.16.sh
libplayit2.sh
play-hand-of-fate.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-hand-of-fate.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
