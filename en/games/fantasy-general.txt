====== Fantasy General ======

===== Description =====

[[https://www.dotslashplay.it/images/games/fantasy-general/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/fantasy-general/thumbnail.jpg?nocache }}]]

**fantasy General**: For more than a century, the world of Aer has known peace, joy and prosperity. Thanks to technological and magical advances, its inhabitants have created peaceful societies free of disease, destruction and evil. Everything was fine…

  * category: Strategy, Turn-Based, Fantasy
  * release date: 1996

===== Informations =====

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-fantasy-general.sh|play-fantasy-general.sh]] (updated on 28/12/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * targets:
    * English version:
      * [[https://www.gog.com/game/fantasy_general|gog_fantasy_general_2.0.0.8.sh]] (MD5: 59b86b9115ae013d2e23a8b4b7b771fd)
    * French version:
      * [[https://www.gog.com/game/fantasy_general|gog_fantasy_general_french_2.0.0.8.sh]] (MD5: 1b188304b4cca838e6918ca6e2d9fe2b)
  * dependencies:
    * Arch Linux:
      * libarchive
    * Debian:
      * bsdtar
      * fakeroot

<note>The game installed via these scripts will run through [[https://www.dosbox.com/|DOSBox]].</note>

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
# pacman -S libarchive
</code>
    - Debian:<code>
# apt-get install bsdtar fakeroot
</code>
  - Put in a same directory the scripts and archive:
    - English version:<code>
$ ls
</code><code>
gog_fantasy_general_2.0.0.8.sh
libplayit2.sh
play-fantasy-general.sh
</code>
    - French version:<code>
$ ls
</code><code>
gog_fantasy_general_french_2.0.0.8.sh
libplayit2.sh
play-fantasy-general.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-fantasy-general.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
