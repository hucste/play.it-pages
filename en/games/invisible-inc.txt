====== Invisible Inc. ======

===== Description =====

[[https://www.dotslashplay.it/images/games/invisible-inc/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/invisible-inc/thumbnail.jpg?nocache }}]]

**Invisible Inc**: Take control of Invisible’s agents in the field and infiltrate the world’s most dangerous corporations.\\
Stealth, precision, and teamwork are essential in high-stakes, high-profit missions, where every move may cost an agent their life.

  * age: 
  * category: RTS, Infiltration
  * official url: [[https://www.klei.com/games/invisible-inc|]]
  * release date: 2015

===== Informations =====

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-invisible-inc.sh|play-invisible-inc.sh]] (updated on 30/01/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * target:
    * [[https://www.gog.com/game/invisible_inc|invisible_inc_en_8_07_2017_15873.sh]] (MD5: b3acb8f72cf01f71b0ddcb4355543a16)
    * [[https://www.dotslashplay.it/ressources/invisible-inc/|invisible-inc_icons.tar.gz]] (MD5: 37a62fed1dc4185e95db3e82e6695c1d)
  * dependencies:
    * Arch Linux:
      * unzip
    * Debian:
      * unzip
      * fakeroot

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
# pacman -S unzip
</code>
    - Debian:<code>
# apt-get install unzip fakeroot
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
invisible_inc_en_8_07_2017_15873.sh
invisible-inc_icons.tar.gz
libplayit2.sh
play-invisible-inc.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-invisible-inc.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
