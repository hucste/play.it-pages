====== Tower of Guns ======
//version sold on Humble Bundle//

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-tower-of-guns_humble-2016-04-01.sh|play-tower-of-guns_humble-2016-04-01.sh]] (updated on 21/04/2016)
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-anything.sh|play-anything.sh]] (updated on 18/03/2018)
  * target:
    * TowerOfGuns-Linux-1.27-2015021101-g_fix.sh (MD5: 45fae40e529e678c9129f9ee2dc8694b)
  * dependencies:
    * fakeroot
    * unzip

[[https://www.dotslashplay.it/images/games/tower-of-guns/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/tower-of-guns/thumbnail.jpg?nocache }}]]

==== Usage ====

  - Install the scripts dependencies:<code>
# apt-get install fakeroot unzip
</code>
  - Put in a same directory the scripts and installer:<code>
$ ls
</code><code>
play-anything.sh
play-tower-of-guns_humble-2016-04-01.sh
TowerOfGuns-Linux-1.27-2015021101-g_fix.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-tower-of-guns_humble-2016-04-01.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>

==== Advanced usage ====

This script accept several options to set its behaviour. Follow the links to more detai$ ls on how to use them.

  * [[:en:common:advanced-options#checksum|checksum]]
  * [[:en:common:advanced-options#compression|compression]]
  * [[:en:common:advanced-options#help|help]]
  * [[:en:common:advanced-options#prefix|prefix]]

==== Links ====

  * [[http://www.towerofguns.com/|official website]]
  * [[https://en.wikipedia.org/wiki/Tower_of_Guns|Wikipedia article]]
