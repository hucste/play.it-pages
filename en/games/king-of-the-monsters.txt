====== King of the Monsters ======
//version sold on Humble Bundle//

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-king-of-the-monsters_humble.sh|play-king-of-the-monsters_humble.sh]] (updated on 21/04/2016)
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-anything.sh|play-anything.sh]] (updated on 18/03/2018)
  * target:
    * KingOfTheMonsters.sh (MD5: bd1e10ee3afe6a7a7b6f3b8564b96bf0)
  * dependencies:
    * fakeroot
    * unzip

[[https://www.dotslashplay.it/images/games/king-of-the-monsters/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/king-of-the-monsters/thumbnail.jpg?nocache }}]]

==== Usage ====

  - Install the scripts dependencies:<code>
# apt-get install fakeroot unzip
</code>
  - Put in a same directory the scripts and installer:<code>
$ ls
</code><code>
KingOfTheMonsters.sh
play-anything.sh
play-king-of-the-monsters_humble.sh
</code>
  - Run the building process from this directory:<code>
$ sh ./play-king-of-the-monsters_humble.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>

==== Advanced usage ====

This script accept several options to set its behaviour. Follow the links to more details on how to use them.

  * [[:en:common:advanced-options#checksum|checksum]]
  * [[:en:common:advanced-options#compression|compression]]
  * [[:en:common:advanced-options#help|help]]
  * [[:en:common:advanced-options#prefix|prefix]]
