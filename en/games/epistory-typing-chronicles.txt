====== Epistory - Typing Chronicles ======

===== Description =====

[[https://www.dotslashplay.it/images/games/epistory-typing-chronicles/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/epistory-typing-chronicles/thumbnail.jpg?nocache }}]]

**Epistory - Typing Chronicles** is a beautiful atmospheric 3D action/adventure typing game that tells the story of a writer lacking inspiration who asks her muse to help write her latest book.

  * age: 7y+
  * category: action, adventure, fantasy
  * official url: [[http://www.epistorygame.com/|]]
  * release date: 2016

===== Informations =====

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-epistory.sh|play-epistory.sh]] (updated on 7/12/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * target:
    * [[https://www.gog.com/game/epistory_typing_chronicles|gog_epistory_typing_chronicles_2.2.0.3.sh]] (MD5: 8db1f835a9189099e57c174ba2353f53)
  * dependencies:
    * Arch Linux:
      * libarchive
    * Debian:
      * bsdtar
      * fakeroot

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
# pacman -S libarchive
</code>
    - Debian:<code>
# apt-get install bsdtar fakeroot
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
gog_epistory_typing_chronicles_2.2.0.3.sh
libplayit2.sh
play-epistory.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-epistory.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
