====== Dreamfall Chapters ======

===== Description =====

[[https://www.dotslashplay.it/images/games/dreamfall-chapters/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/dreamfall-chapters/thumbnail.jpg?nocache }}]]

**Dreamfall Chapters** takes players on an epic journey, from the future cyberpunk and dystopique of Stark to the mysterious and dreamlike Storytime, passing through the magical landscapes of Arcadia.\\
Through a narrative of faith, hope and change about the choices we make and the individuals that fate makes of us, players will explore these fantasy worlds and mature themes through the eyes of three playable characters.

  * age: 16y+
  * category: adventure
  * official url: [[https://www.redthreadgames.com/dreamfall-chapters|]]
  * release date: 2014

===== Informations =====

//version sold on GOG//

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-dreamfall-chapters.sh|play-dreamfall-chapters.sh]] (updated on 21/07/2016)
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-anything.sh|play-anything.sh]] (updated on 18/03/2018)
  * target:
    * gog_dreamfall_chapters_2.19.0.23.sh (MD5: 3f05c530a0e07b7227e3fb7b6601e19a)
  * dependencies:
    * fakeroot
    * unzip

==== Usage ====

  - Install the scripts dependencies:<code>
# apt-get install fakeroot unzip
</code>
  - Put in a same directory scripts and installer:<code>
$ ls
</code><code>
gog_dreamfall_chapters_2.19.0.23.sh
play-anything.sh
play-dreamfall-chapters.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-dreamfall-chapters.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>

==== Advanced usage ====

This script accept several options to set its behaviour. Follow the links to more detai$ ls on how to use them.

  * [[:en:common:advanced-options#checksum|checksum]]
  * [[:en:common:advanced-options#compression|compression]]
  * [[:en:common:advanced-options#help|help]]
  * [[:en:common:advanced-options#prefix|prefix]]

==== Links ====

  * [[https://en.wikipedia.org/wiki/Dreamfall_Chapters:_The_Longest_Journey|Wikipedia article]]
