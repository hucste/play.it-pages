====== Tulpa ======

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-tulpa.sh|play-tulpa.sh]] (updated on 15/01/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * target:
    * [[https://www.humblebundle.com/store/tulpa|Tulpa_Linux_1423847478.zip]] (MD5: 3e01614c5c1c562aaf423689a1f51df9)
  * dependencies:
    * Arch Linux:
      * unzip
    * Debian:
      * fakeroot
      * unzip

[[https://www.dotslashplay.it/images/games/tulpa/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/tulpa/thumbnail.jpg?nocache }}]]

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
# pacman -S unzip
</code>
    - Debian:<code>
# apt-get install fakeroot unzip
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
libplayit2.sh
play-tulpa.sh
Tulpa_Linux_1423847478.zip
</code>
  - Start the building process from this directory:<code>
$ sh ./play-tulpa.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
