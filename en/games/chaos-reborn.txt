====== Chaos Reborn ======

===== Description =====

[[https://www.dotslashplay.it/images/games/chaos-reborn/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/chaos-reborn/thumbnail.jpg?nocache }}]]

**Chaos Reborn**: You play the role of a fighting wizard armed with a selection of spells and magical staff.\\
Some spells summon creatures but others are more exotic.\\
The balance between Law and Chaos can be altered to your advantage, or you can cast illusions to deceive your adversaries.\\
It’s a game of bluff, tactics, and calculated risks with nail-biting decisions from one turn to the next.

  * category: RPG, strategy, turn-based
  * official url: [[http://www.chaos-reborn.com/|]]
  * release date: 2015

===== Informations =====

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-chaos-reborn.sh|play-chaos-reborn.sh]] (updated on 26/12/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * target:
    * [[https://www.gog.com/game/chaos_reborn|chaos_reborn_en_1_13_2_17223.sh]] (MD5: edb60d98710c87c0adea06f55be99567)
  * dependencies:
    * Arch Linux:
      * libarchive
    * Debian:
      * bsdtar
      * fakeroot

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
# pacman -S libarchive
</code>
    - Debian:<code>
# apt-get install bsdtar fakeroot
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
chaos_reborn_en_1_13_2_17223.sh
libplayit2.sh
play-chaos-reborn.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-chaos-reborn.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
