====== Dreaming Sarah ======

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-dreaming-sarah.sh|play-dreaming-sarah.sh]] (mis à jour le 10/03/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 2/04/2018)
  * cible :
    * 32-bit :
      * [[https://www.humblebundle.com/store/dreaming-sarah|DreamingSarah-linux32_1.3.zip]] (MD5 : 73682a545e979ad9a2b6123222ddb517)
    * 64-bit :
      * [[https://www.humblebundle.com/store/dreaming-sarah|DreamingSarah-linux64_1.3.zip]] (MD5 : a68f3956eb09ea7b34caa20f6e89b60c)
  * dépendances :
    * Arch Linux :
      * unzip
    * Debian :
      * fakeroot
      * unzip

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
# pacman -S unzip
</code>
    - Debian :<code>
# apt-get install fakeroot
</code>
  - Placez dans un même répertoire les scripts et l’archive :
    - 32-bit :<code>
$ ls
</code><code>
libplayit2.sh
play-dreaming-sarah.sh
DreamingSarah-linux32_1.3.zip
</code>
    - 64-bit :<code>
$ ls
</code><code>
libplayit2.sh
play-dreaming-sarah.sh
DreamingSarah-linux64_1.3.zip
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-dreaming-sarah.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
