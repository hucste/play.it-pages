====== Blackwell 1: The Blackwell Legacy ======

===== Description =====

**The Blackwell Legacy** : Quand le seul parent de Rosa Blackwell meurt après vingt ans dans le coma, elle pense que le pire est passé.\\ 
Tout change lorsque Joey Mallone, un fantôme sardonique des années 1930, entre dans sa vie et lui dit qu’elle est médium.\\
Qu’ils le veuillent ou non, c’est à eux de guérir les maux surnaturels de New York.\\
Le premier cas du duo portera sur une série de suicides dans une université locale.\\
Quelque chose d’anormal a forcé ces étudiants à se suicider, et personne ne sait pourquoi.

  * age : +12 ans
  * catégorie : action, ésoterisme, Point'n Click
  * url officielle : [[http://www.wadjeteyegames.com/games/blackwell-legacy/|]]
  * date de sortie : 2006

===== Informations =====

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-blackwell-1-the-blackwell-legacy.sh|play-blackwell-1-the-blackwell-legacy.sh]] (mis à jour le 15/01/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 2/04/2018)
  * cible :
    * [[https://www.gog.com/game/blackwell_bundle|gog_blackwell_legacy_2.0.0.2.sh]] (MD5 : f16c09c0ca29b579a0d87c9be7361375)
  * dépendances :
    * Arch Linux :
      * libarchive
    * Debian :
      * bsdtar
      * fakeroot

[[https://www.dotslashplay.it/images/games/blackwell-1-the-blackwell-legacy/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/blackwell-1-the-blackwell-legacy/thumbnail.jpg?nocache }}]]

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
# pacman -S libarchive
</code>
    - Debian :<code>
# apt-get install bsdtar fakeroot
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
libplayit2.sh
gog_blackwell_legacy_2.0.0.2.sh
play-blackwell-1-the-blackwell-legacy.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-blackwell-1-the-blackwell-legacy.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
