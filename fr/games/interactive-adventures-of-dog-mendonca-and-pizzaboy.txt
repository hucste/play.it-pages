====== The Interactive Adventures of Dog Mendonça and Pizzaboy ======

===== Description =====

[[https://www.dotslashplay.it/images/games/the-interactive-adventures-of-dog-mendonca-and-pizzaboy/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/the-interactive-adventures-of-dog-mendonca-and-pizzaboy/thumbnail.jpg?nocache }}]]

**The Interactive Adventures of Dog Mendonça and Pizzaboy** :  Mettez-vous dans la peau d'Eurico, un ancien livreur de pizza et apprenti sans rémunération du légendaire détective occulte Dog Mendonça, vous découvrez alors les mystères du monde souterrain surnaturel de Lisbonne.\\
Lorsque des monstres qui vivent secrètement côte à côte avec des humains commencent à disparaître, c'est à Eurico et son équipe de détectives paranormaux de découvrir la vérité.\\
Déchiffrez l'une des plus grandes conspirations de notre temps, sauvez la journée et gagnez peut-être même l'argent comptant, dans ce chèque bien mérité !

  * catégorie : aventure, pointer et cliquer, mystère
  * url officielle : [[http://www.dog-mendonca-game.com|]]
  * date de sortie : 2016

===== Informations =====

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-the-interactive-adventures-of-dog-mendonca-and-pizzaboy.sh|play-the-interactive-adventures-of-dog-mendonca-and-pizzaboy.sh]] (mis à jour le 3/02/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 2/04/2018)
  * cible :
    * [[https://www.gog.com/game/the_interactive_adventures_of_dog_mendonca_and_pizzaboy|gog_the_interactive_adventures_of_dog_mendon_a_and_pizzaboy_2.0.0.1.sh]] (MD5 : 88c5bb7a410fbf2eb0cc124c3ce06fa0)
  * dépendances :
    * Arch Linux :
      * libarchive
    * Debian :
      * bsdtar
      * fakeroot

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
# pacman -S libarchive
</code>
    - Debian :<code>
# apt-get install bsdtar fakeroot
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
libplayit2.sh
play-the-interactive-adventures-of-dog-mendonca-and-pizzaboy.sh
gog_the_interactive_adventures_of_dog_mendon_a_and_pizzaboy_2.0.0.1.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-the-interactive-adventures-of-dog-mendonca-and-pizzaboy.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
