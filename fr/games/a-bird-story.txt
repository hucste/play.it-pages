====== A Bird Story ======

===== Description =====

[[https://www.dotslashplay.it/images/games/a-bird-story/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/a-bird-story/thumbnail.jpg?nocache }}]]

**A Bird Story** permet de suivre l’aventure d’un écolier solitaire. Celui-ci sauve un oiseau qui ne peut plus voler, s’ensuit une grande histoire sentimentale.

  * catégorie : aventure
  * url officielle : [[http://freebirdgames.com/games/a-bird-story/|]]
  * date de sortie : 2014

===== Informations =====

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-a-bird-story.sh|play-a-bird-story.sh]] (mis à jour le 2/01/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 2/04/2018)
  * cible :
    * [[https://www.gog.com/game/a_bird_story|gog_a_bird_story_2.0.0.3.sh]] (MD5 : 6f334da4493e8c050a16d4b66987d3ff)
  * dépendances :
    * Arch Linux :
      * libarchive
    * Debian :
      * bsdtar
      * fakeroot

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
# pacman -S libarchive
</code>
    - Debian :<code>
# apt-get install bsdtar fakeroot
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
gog_a_bird_story_2.0.0.3.sh
libplayit2.sh
play-a-bird-story.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-a-bird-story.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
