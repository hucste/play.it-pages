====== Metal Slug 2 ======
//version vendue sur Humble Bundle//

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-metal-slug-2_humble.sh|play-metal-slug-2_humble.sh]] (mis à jour le 22/04/2016)
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-anything.sh|play-anything.sh]] (mis à jour le 18/03/2018)
  * cible :
    * MetalSlug2.sh (MD5 : db3871638d61738d7473fb8b481f651c)
  * dépendances :
    * fakeroot
    * unzip

[[https://www.dotslashplay.it/images/games/metal-slug-2/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/metal-slug-2/thumbnail.jpg?nocache }}]]

==== Utilisation ====

  - Installez les dépendances des scripts :<code>
# apt-get install fakeroot unzip
</code>
  - Placez dans un même répertoire les scripts et l’installeur :<code>
$ ls
</code><code>
MetalSlug2.sh
play-anything.sh
play-metal-slug-2_humble.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-metal-slug-2_humble.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>

==== Utilisation avancée ====

Le script présenté ici peut prendre plusieurs options pour contrôler son comportement de manière plus fine. Suivez les liens pour avoir des détails sur leur utilisation.

  * [[:fr:commun:options-avancees#checksum|checksum]]
  * [[:fr:commun:options-avancees#compression|compression]]
  * [[:fr:commun:options-avancees#help|help]]
  * [[:fr:commun:options-avancees#prefix|prefix]]

==== Liens ====

[[https://en.wikipedia.org/wiki/Metal_Slug_2|article Wikipedia]] (en anglais)
