====== Zork III: The Dungeon Master ======

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-zork-3.sh|play-zork-3.sh]] (mis à jour le 23/05/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 2/04/2018)
  * cible :
    * [[https://www.gog.com/game/the_zork_anthology|setup_zork3_2.1.0.17.exe]] (MD5 : 1526e9be21bf47412dc053f4097e25bd)
  * dépendances :
    * Arch Linux :
      * icoutils
      * innoextract
    * Debian :
      * fakeroot
      * icoutils
      * innoextract

[[https://www.dotslashplay.it/images/games/zork-3/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/zork-3/thumbnail.jpg?nocache }}]]

<note>Le jeu installé via ce script utilisera [[https://www.dosbox.com/|DOSBox]].</note>

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
$ yaourt -S icoutils innoextract
</code>
    - Debian :<code>
# apt-get install fakeroot icoutils innoextract
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
libplayit2.sh
play-zork-3.sh
setup_zork3_2.1.0.17.exe
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-zork-3.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>

==== Liens ====

  * [[https://fr.wikipedia.org/wiki/Zork_III:_The_Dungeon_Master|article Wikipédia]]
