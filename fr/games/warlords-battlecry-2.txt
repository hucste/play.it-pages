====== Warlords Battlecry II ======
//version vendue sur GOG//

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-warlords-battlecry-2_gog-2.0.0.4.sh|play-warlords-battlecry-2_gog-2.0.0.4.sh]] (mis à jour le 28/12/2016)
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-anything.sh|play-anything.sh]] (mis à jour le 18/03/2018)
  * cible :
    * setup_warlords_battlecry2_2.0.0.4.exe (MD5 : baa54ca0285182d18d532abfcbb8769f)
  * dépendances :
    * fakeroot
    * innoextract
    * icoutils (optionnel)

[[https://www.dotslashplay.it/images/games/warlords-battlecry-2/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/warlords-battlecry-2/thumbnail.jpg?nocache }}]]

<note>Le jeu installé via ce script utilisera [[https://www.winehq.org/|WINE]].</note>

==== Utilisation ====

  - Installez les dépendances des scripts :<code>
# apt-get install fakeroot icoutils innoextract
</code>
  - Placez dans un même répertoire les scripts et l’installeur :<code>
$ ls
</code><code>
play-warlords-battlecry-2_gog-2.0.0.4.sh
play-anything.sh
setup_warlords_battlecry2_2.0.0.4.exe
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-warlords-battlecry-2_gog-2.0.0.4.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>

==== Utilisation avancée ====

Le script présenté ici peut prendre plusieurs options pour contrôler son comportement de manière plus fine. Suivez les liens pour avoir des détails sur leur utilisation.

  * [[:fr:commun:options-avancees#checksum|checksum]]
  * [[:fr:commun:options-avancees#compression|compression]]
  * [[:fr:commun:options-avancees#help|help]]
  * [[:fr:commun:options-avancees#prefix|prefix]]

==== Configurer le préfixe WINE ====

Vous pouvez accéder à la fenêtre de configuration du préfixe WINE dédié au jeu en lançant la commande suivante :
<code>
wbc2-winecfg
</code>
Toute modification effectuée via cette fenêtre sera spécifique à ce jeu, sans influence pour vos autres jeux WINE, qu’ils aient été installé via un script ./play.it ou un appel direct à WINE.

==== Lancer le jeu dans une fenêtre ====

La méthode suivante va vous permettre de lancer le jeu en mode fenêtré facilement :
  - Ouvrez la fenêtre de configuration du préfixe WINE, via la commande donnée plus haut ;
  - Dans la fenêtre qui s’affiche, allez sur l’onglet "Affichage" ;
  - Cochez la case "Émuler un bureau virtuel", et dans les champs "Taille du bureau" renseignez la résolution qu’utilise votre bureau ;
  - Fermez la fenêtre avec "OK".
C’est fait, Warlords Battlecry II se lancera maintenant dans un fenêtre adaptée à la résolution qu’il utilise. Vous pouvez à tout moment revenir à un mode plein écran en retournant dans la fenêtre de configuration de WINE et en décochant la case "Émuler un bureau virtuel".

==== Liens ====

  * [[https://fr.wikipedia.org/wiki/Warlords_Battlecry_II|Warlords Battlecry II sur Wikipédia]]
  * [[https://appdb.winehq.org/objectManager.php?sClass=version&iId=1168|entrée dans la base de donnée WineHQ AppDB]] (en anglais)
