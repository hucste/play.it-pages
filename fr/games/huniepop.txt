====== HuniePop ======

===== Description =====

[[https://www.dotslashplay.it/images/games/huniepop/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/huniepop/thumbnail.jpg?nocache }}]]

**HuniePop** est une expérience sim unique…\\
C'est une première approche de gameplay qui est à la fois une simulation de rencontre, un jeu de puzzle, avec des éléments RPG légers, un style de présentation visuel original, un style d'écriture western abrasif et beaucoup d'intrigue.

  * age : +18 ans
  * catégorie : adulte, contenu sexuel, nudité, sim, puzzle, RPG
  * url officielle : [[http://www.huniepot.com|]]
  * date de sortie : 2015

===== Informations =====

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-huniepop.sh|play-huniepop.sh]] (mis à jour le 10/02/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 2/04/2018)
  * cible :
    * [[https://www.gog.com/game/huniepop|gog_huniepop_2.0.0.3.sh]] (MD5 : d229aea2b601137537f7be46c7327660)
  * dépendances :
    * Arch Linux :
      * libarchive
    * Debian :
      * bsdtar
      * fakeroot

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
# pacman -S libarchive
</code>
    - Debian :<code>
# apt-get install bsdtar fakeroot
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
gog_huniepop_2.0.0.3.sh
libplayit2.sh
play-huniepop.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-huniepop.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
