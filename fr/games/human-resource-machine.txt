====== Human Resource Machine ======

===== Description =====

[[https://www.dotslashplay.it/images/games/human-resource-machine/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/human-resource-machine/thumbnail.jpg?nocache }}]]

**Human Resource Machine** :  Programmez de petits employés de bureau à résoudre des énigmes.\\
Soyez un bon employé ! Les machines viennent… pour votre travail.

  * catégorie : puzzle
  * url officielle : [[http://tomorrowcorporation.com/humanresourcemachine|]]
  * date de sortie : 2015

===== Informations =====

//version vendue sur GOG//

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-human-resource-machine_gog-2.0.0.2.sh|play-human-resource-machine_gog-2.0.0.2.sh]] (mis à jour le 9/05/2016)
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-anything.sh|play-anything.sh]] (mis à jour le 18/03/2018)
  * cible :
    * gog_human_resource_machine_2.0.0.2.sh (MD5 : 35bc19839c73ddf4b503c58a0a887f98)
  * dépendances :
    * fakeroot
    * unzip

==== Utilisation ====

  - Installez les dépendances des scripts :<code>
# apt-get install fakeroot unzip
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
gog_human_resource_machine_2.0.0.2.sh
play-anything.sh
play-human-resource-machine_gog-2.0.0.2.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-human-resource-machine_gog-2.0.0.2.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>

==== Utilisation avancée ====

Le script présenté ici peut prendre plusieurs options pour contrôler son comportement de manière plus fine. Suivez les liens pour avoir des détails sur leur utilisation.

  * [[:fr:commun:options-avancees#checksum|checksum]]
  * [[:fr:commun:options-avancees#compression|compression]]
  * [[:fr:commun:options-avancees#help|help]]
  * [[:fr:commun:options-avancees#prefix|prefix]]

==== Liens ====

  * [[https://en.wikipedia.org/wiki/Human_Resource_Machine|article Wikipedia]] (en anglais)
