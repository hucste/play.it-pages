====== Pier Solar and the Great Architects ======
//version vendue sur GOG//

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-pier-solar_gog-2.1.0.4.sh|play-pier-solar_gog-2.1.0.4.sh]] (mis à jour le 12/06/2016)
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-anything.sh|play-anything.sh]] (mis à jour le 18/03/2018)
  * cible :
    * gog_pier_solar_and_the_great_architects_2.1.0.4.sh (MD5 : 2de03fb6d69944e3f204d5ae45147a3e)
  * dépendances :
    * fakeroot
    * unzip

[[https://www.dotslashplay.it/images/games/pier-solar-hd/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/pier-solar-hd/thumbnail.jpg?nocache }}]]

==== Utilisation ====

  - Installez les dépendances des scripts :<code>
# apt-get install fakeroot unzip
</code>
  - Placez dans un même répertoire les scripts et l’installeur :<code>
$ ls
</code><code>
gog_pier_solar_and_the_great_architects_2.1.0.4.sh
play-anything.sh
play-pier-solar_gog-2.1.0.4.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-pier-solar_gog-2.1.0.4.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>

==== Utilisation avancée ====

Le script présenté ici peut prendre plusieurs options pour contrôler son comportement de manière plus fine. Suivez les liens pour avoir des détails sur leur utilisation.

  * [[:fr:commun:options-avancees#checksum|checksum]]
  * [[:fr:commun:options-avancees#compression|compression]]
  * [[:fr:commun:options-avancees#help|help]]
  * [[:fr:commun:options-avancees#prefix|prefix]]

==== Liens ====

  * [[https://fr.wikipedia.org/wiki/Pier_Solar_and_the_Great_Architects|article Wikipédia]]
