====== No Pineapple Left Behind ======

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-no-pineapple-left-behind.sh|play-no-pineapple-left-behind.sh]] (mis à jour le 24/08/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 2/04/2018)
  * cible :
    * [[https://www.gog.com/game/no_pineapple_left_behind|gog_no_pineapple_left_behind_2.4.0.7.sh]] (MD5 : 8134abbdbc068f3305a54a41f32820bc)
  * dépendances :
    * Arch Linux :
      * libarchive
    * Debian :
      * bsdtar
      * fakeroot

[[https://www.dotslashplay.it/images/games/no-pineapple-left-behind/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/no-pineapple-left-behind/thumbnail.jpg?nocache }}]]

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
# pacman -S libarchive
</code>
    - Debian :<code>
# apt-get install bsdtar fakeroot
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
gog_no_pineapple_left_behind_2.4.0.7.sh
libplayit2.sh
play-no-pineapple-left-behind.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-no-pineapple-left-behind.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
