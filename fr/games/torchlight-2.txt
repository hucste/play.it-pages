====== Torchlight II ======

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-torchlight-2.sh|play-torchlight-2.sh]] (mis à jour le 8/12/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 2/04/2018)
  * cibles :
    * GOG :
      * [[https://www.gog.com/game/torchlight_ii|gog_torchlight_2_2.0.0.2.sh]] (MD5 : e107f6d4c6d4cecea37ade420a8d4892)
    * Humble Bundle :
      * [[https://www.humblebundle.com/store/torchlight-ii|Torchlight2-linux-2015-04-01.sh]] (MD5 : 730a5d08c8f1cd4a65afbc0ca631d85c)
  * dépendances :
    * Arch Linux :
      * libarchive
    * Debian :
      * bsdtar
      * fakeroot

[[https://www.dotslashplay.it/images/games/torchlight-2/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/torchlight-2/thumbnail.jpg?nocache }}]]

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
# pacman -S libarchive
</code>
    - Debian :<code>
# apt-get install bsdtar fakeroot
</code>
  - Placez dans un même répertoire les scripts et l’archive :
    - GOG :<code>
$ ls
</code><code>
gog_torchlight_2_2.0.0.2.sh
libplayit2.sh
play-torchlight-2.sh
</code>
    - Humble Bundle :<code>
$ ls
</code><code>
libplayit2.sh
play-torchlight-2.sh
Torchlight2-linux-2015-04-01.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-torchlight-2.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
