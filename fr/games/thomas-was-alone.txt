====== Thomas Was Alone ======

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-thomas-was-alone.sh|play-thomas-was-alone.sh]] (mis à jour le 24/08/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 2/04/2018)
  * cible :
    * thomaswasalone-linux-1369349552.tar (MD5 : e7f8e766188718e16880b1137c430f35)
  * dépendances :
    * Debian :
      * fakeroot

[[https://www.dotslashplay.it/images/games/thomas-was-alone/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/thomas-was-alone/thumbnail.jpg?nocache }}]]

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Debian :<code>
# apt-get install fakeroot
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
libplayit2.sh
play-thomas-was-alone.sh
thomaswasalone-linux-1369349552.tar
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-thomas-was-alone.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
