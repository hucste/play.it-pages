====== Arcanum : Engrenages et Sortilèges ======

===== Description =====

[[https://www.dotslashplay.it/images/games/arcanum/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/arcanum/thumbnail.jpg?nocache }}]]

**Arcanum** : Choisissez la magie ou la technologie, et combattez des monstres, soit en temps réel, soit au tour par tour.

  * age : +12 ans
  * catégorie : RPG
  * date de sortie : 2001

===== Informations =====

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-arcanum.sh|play-arcanum.sh]] (mis à jour le 31/03/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 2/04/2018)
  * cible :
    * setup_arcanum_2.0.0.15.exe (MD5 : c09523c61edd18abb97da97463e07a88)
  * dépendances :
    * Arch Linux :
      * icoutils
      * innoextract
    * Debian :
      * fakeroot
      * icoutils
      * innoextract

<note>Le jeu installé via ce script utilisera [[https://www.winehq.org/|WINE]].</note>

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
$ yaourt icoutils innoextract
</code>
    - Debian :<code>
# apt-get install fakeroot icoutils innoextract
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
libplayit2.sh
play-arcanum.sh
setup_arcanum_2.0.0.15.exe
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-arcanum.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
