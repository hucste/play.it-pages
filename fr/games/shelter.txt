====== Shelter ======

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-shelter.sh|play-shelter.sh]] (mis à jour le 6/12/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 2/04/2018)
  * cible :
    * [[https://www.gog.com/game/shelter|setup_shelter_2.0.0.6.exe]] (MD5 : 06860af1df9a8120bce4e97d899b3edd)
  * dépendances :
    * Arch Linux :
      * icoutils
      * innoextract
    * Debian :
      * fakeroot
      * icoutils
      * innoextract

[[https://www.dotslashplay.it/images/games/shelter/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/shelter/thumbnail.jpg?nocache }}]]

<note>Le jeu installé via ce script utilisera [[https://www.winehq.org/|WINE]].</note>

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
$ yaourt icoutils innoextract
</code>
    - Debian :<code>
# apt-get install fakeroot icoutils innoextract
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
libplayit2.sh
play-shelter.sh
setup_shelter_2.0.0.6.exe
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-shelter.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
