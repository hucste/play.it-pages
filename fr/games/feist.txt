====== Feist ======

===== Description =====

[[https://www.dotslashplay.it/images/games/feist/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/feist/thumbnail.jpg?nocache }}]]

**Feist** : autour d’une petite créature piégée dans un monde hostile et mystérieux, suivez d’étranges bêtes dans des forêts assoupies, des montagnes périlleuses, des cavernes obscures et des marais lumineux.\\
Prenez garde aux créatures dangereuses qui réagiront à vos mouvements et décisions avec leur I.A. dynamique, et aux pièges aussi redoutables qu’adroitement dissimulés.

  * age : +10 ans
  * catégorie : action, Fantasy
  * url officielle : [[https://playfeist.net|]]
  * date de sortie : 2015

===== Informations =====

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-feist.sh|play-feist.sh]] (mis à jour le 3/02/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 2/04/2018)
  * cible :
    * [[https://www.gog.com/game/feist|gog_feist_2.4.0.7.sh]] (MD5 : ce0b128c23defc946535ec180dcef121)
  * dépendances :
    * Arch Linux :
      * libarchive
    * Debian :
      * bsdtar
      * fakeroot

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
# pacman -S libarchive
</code>
    - Debian :<code>
# apt-get install bsdtar fakeroot
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
libplayit2.sh
play-feist.sh
gog_feist_2.4.0.7.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-feist.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
