====== Kingdom New Lands ======

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-kingdom-new-lands.sh|play-kingdom-new-lands.sh]] (mis à jour le 30/12/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 2/04/2018)
  * cible :
    * [[https://www.gog.com/game/kingdom_new_lands|gog_kingdom_new_lands_2.6.0.8.sh]] (MD5 : 0d662366f75d5da214e259d792e720eb)
  * dépendances :
    * Arch Linux :
      * libarchive
    * Debian :
      * bsdtar
      * fakeroot

[[https://www.dotslashplay.it/images/games/kingdom-new-lands/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/kingdom-new-lands/thumbnail.jpg?nocache }}]]

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
# pacman -S libarchive
</code>
    - Debian :<code>
# apt-get install bsdtar fakeroot
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
gog_kingdom_new_lands_2.6.0.8.sh
libplayit2.sh
play-kingdom-new-lands.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-kingdom-new-lands.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
