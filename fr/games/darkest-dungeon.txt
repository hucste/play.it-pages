====== Darkest Dungeon ======

===== Description =====

[[https://www.dotslashplay.it/images/games/darkest-dungeon/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/darkest-dungeon/thumbnail.jpg?nocache }}]]

  * age : +16 ans
  * catégorie : RPG, Fantasy, tour-à-tour
  * url officielle : [[http://www.darkestdungeon.com/|]]
  * date de sortie : 2016

===== Informations =====

=== Darkest Dungeon ===

**Darkest Dungeon**: Recrutez, entraînez et menez une équipe de héros avec leurs qualités et leurs défauts (surtout leurs défauts) à travers des forêts maudites, des labyrinthes oubliés, des cryptes délabrées et bien pire.\\ 
Vous affronterez non seulement des adversaires redoutables mais également le stress, la famine, les maladies et les ténèbres.\\ 
Découvrez d’étranges secrets et venez à bout de monstres terrifiants en utilisant le système de combat stratégique.

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-darkest-dungeon.sh|play-darkest-dungeon.sh]] (mis à jour le 4/11/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 2/04/2018)
  * cible :
    * [[https://www.gog.com/game/darkest_dungeon|darkest_dungeon_en_21142_16140.sh]] (MD5 : 4b43065624dbab74d794c56809170588)
  * dépendances :
    * Arch Linux:
      * libarchive
    * Debian:
      * bsdtar
      * fakeroot

=== The Crimson Court ===

**Darkest Dungeon : The Crimson Court** est la première extension du jeu de rôle gothique.\\
Crimson Court propose une campagne inédite, en parallèle du contenu principal de Darkest Dungeon, incluant de nouveaux défis et différents niveaux de difficulté.\\
L’histoire, qui explore le passé de l’ancêtre, se présente sous la forme de cinématiques comme le reste du jeu.

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-darkest-dungeon-the-crimson-court.sh|play-darkest-dungeon-the-crimson-court.sh]] (mis à jour le 1/11/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 2/04/2018)
  * cible :
    * [[https://www.gog.com/game/darkest_dungeon_the_crimson_court|darkest_dungeon_the_crimson_court_dlc_en_21096_16065.sh]] (MD5 : d4beaeb7effff0cbd2e292abf0ef5332)
  * dépendances :
    * Arch Linux:
      * libarchive
    * Debian:
      * bsdtar
      * fakeroot

=== The Shieldbreaker ===

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-darkest-dungeon-the-shieldbreaker.sh|play-darkest-dungeon-the-shieldbreaker.sh]] (mis à jour le 18/03/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 2/04/2018)
  * cible :
    * [[https://www.gog.com/game/darkest_dungeon_the_shieldbreaker|darkest_dungeon_the_shieldbreaker_dlc_en_21142_16140.sh]] (MD5 : 8606531e5fc728786f497b4803c19994)
  * dépendances :
    * Arch Linux :
      * libarchive
    * Debian :
      * bsdtar
      * fakeroot

==== Utilisation (Darkest Dungeon) ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
# pacman -S libarchive
</code>
    - Debian :<code>
# apt-get install bsdtar fakeroot
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
darkest_dungeon_en_21142_16140.sh
libplayit2.sh
play-darkest-dungeon.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-darkest-dungeon.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>

==== Utilisation (The Crimson Court) ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
# pacman -S libarchive
</code>
    - Debian :<code>
# apt-get install bsdtar fakeroot
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
darkest_dungeon_the_crimson_court_dlc_en_21096_16065.sh
libplayit2.sh
play-darkest-dungeon-the-crimson-court.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-darkest-dungeon-the-crimson-court.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>

==== Utilisation (The Shieldbreaker) ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
# pacman -S libarchive
</code>
    - Debian :<code>
# apt-get install bsdtar fakeroot
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
darkest_dungeon_the_shieldbreaker_dlc_en_21142_16140.sh
libplayit2.sh
play-darkest-dungeon-the-shieldbreaker.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-darkest-dungeon-the-shieldbreaker.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
