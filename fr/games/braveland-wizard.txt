====== Braveland Wizard ======

===== Description =====

[[https://www.dotslashplay.it/images/games/braveland-wizard/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/braveland-wizard/thumbnail.jpg?nocache }}]]

**Braveland Wizard** est le deuxième livre de la série Braveland. Vous êtes diplômé de l’Académie de Magie, prêt pour de nouvelles aventures.\\
Votre voyage commence haut dans les montagnes et traverse les terres du sud, peuplées d'orcs, de fantômes et de mystérieux nomades.

  * catégorie : aventure, TPG, stratégie, tour-à-tour
  * url officielle : [[http://www.tortugateam.com/en/|]]
  * date de sortie : 2014

===== Informations =====

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-braveland-wizard.sh|play-braveland-wizard.sh]] (mis à jour le 8/02/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 2/04/2018)
  * cible :
    * [[https://www.gog.com/game/braveland_wizard|braveland_wizard_en_1_1_3_13_18418.sh]] (MD5 : 7153da6ceb0deda556ebdb7cfa4b9203)
  * dépendances :
    * Arch Linux :
      * libarchive
    * Debian :
      * bsdtar
      * fakeroot

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
# pacman -S libarchive
</code>
    - Debian :<code>
# apt-get install bsdtar fakeroot
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
braveland_wizard_en_1_1_3_13_18418.sh
libplayit2.sh
play-braveland-wizard.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-braveland-wizard.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
