====== The Swapper ======

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-the-swapper.sh|play-the-swapper.sh]] (mis à jour le 30/01/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 2/04/2018)
  * cibles :
    * [[https://www.humblebundle.com/store/the-swapper|the-swapper-linux-1.24_1409159048.sh]] (MD5 : 4f9627d245388edc320f61fae7cbd29f)
    * [[https://www.dotslashplay.it/ressources/the-swapper/|the-swapper_icons.tar.gz]] (MD5 : cddcf271fb6eb10fba870aa91c30c410)
  * dépendances :
    * Arch Linux :
      * libarchive
    * Debian :
      * bsdtar
      * fakeroot

[[https://www.dotslashplay.it/images/games/the-swapper/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/the-swapper/thumbnail.jpg?nocache }}]]

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
# pacman -S libarchive
</code>
    - Debian :<code>
# apt-get install bsdtar fakeroot
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
libplayit2.sh
play-the-swapper.sh
the-swapper_icons.tar.gz
the-swapper-linux-1.24_1409159048.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-the-swapper.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>

==== Liens ====

  * [[http://facepalmgames.com/the-swapper/|site officiel]] (en anglais)
  * [[https://fr.wikipedia.org/wiki/The_Swapper|article Wikipédia]]
