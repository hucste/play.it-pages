====== Epistory - Typing Chronicles ======

===== Description =====

[[https://www.dotslashplay.it/images/games/epistory-typing-chronicles/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/epistory-typing-chronicles/thumbnail.jpg?nocache }}]]

**Epistory - Typing Chronicles** est un beau jeu d’action et d’aventure en 3D qui raconte l’histoire d’une écrivaine sans inspiration qui demande à sa muse de l’aider à écrire son dernier livre.

  * age : +7 ans
  * catégorie : action, aventure, fantasy
  * url officielle : [[http://www.epistorygame.com|]]
  * date de sortie : 2016

===== Informations =====

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-epistory.sh|play-epistory.sh]] (mis à jour le 7/12/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 2/04/2018)
  * cible :
    * [[https://www.gog.com/game/epistory_typing_chronicles|gog_epistory_typing_chronicles_2.2.0.3.sh]] (MD5 : 8db1f835a9189099e57c174ba2353f53)
  * dépendances :
    * Arch Linux :
      * libarchive
    * Debian :
      * bsdtar
      * fakeroot

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
# pacman -S libarchive
</code>
    - Debian :<code>
# apt-get install bsdtar fakeroot
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
gog_epistory_typing_chronicles_2.2.0.3.sh
libplayit2.sh
play-epistory.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-epistory.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
