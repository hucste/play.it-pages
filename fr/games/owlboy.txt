====== Owlboy ======

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-owlboy.sh|play-owlboy.sh]] (mis à jour le 9/12/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 2/04/2018)
  * cible :
    * [[https://www.humblebundle.com/store/owlboy|owlboy-11022017-bin]] (MD5 : d3a1e4753a604431c58eb1ea26c35543)
  * dépendances :
    * Arch Linux :
      * libarchive
    * Debian :
      * bsdtar
      * fakeroot

[[https://www.dotslashplay.it/images/games/owlboy/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/owlboy/thumbnail.jpg?nocache }}]]

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
# pacman -S libarchive
</code>
    - Debian :<code>
# apt-get install bsdtar fakeroot
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
libplayit2.sh
owlboy-11022017-bin
play-owlboy.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-owlboy.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
