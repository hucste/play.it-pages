====== Hollow Knight ======

===== Description =====

[[https://www.dotslashplay.it/images/games/hollow-knight/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/hollow-knight/thumbnail.jpg?nocache }}]]

**Hollow Knight** :  Sous la ville en déclin de Dirtmouth dort un ancien royaume oublié.\\
Beaucoup sont attirés sous la surface, à la recherche de richesses, de gloire ou de réponses à d'anciens secrets.

En tant qu'énigmatique chevalier creux, vous traverserez les profondeurs, démêlerez ses mystères et vaincrez ses maux.

  * catégorie : action, aventure
  * url officielle : [[http://hollowknight.com|]]
  * date de sortie : 2017

===== Informations =====

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-hollow-knight.sh|play-hollow-knight.sh]] (mis à jour le 24/08/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 2/04/2018)
  * cible :
    * [[https://www.gog.com/game/hollow_knight|gog_hollow_knight_2.1.0.2.sh]] (MD5 : 0d18baf29d5552dc094ca2bfe5fcaae6)
  * dépendances :
    * Arch Linux :
      * libarchive
    * Debian :
      * bsdtar
      * fakeroot

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
# pacman -S libarchive
</code>
    - Debian :<code>
# apt-get install bsdtar fakeroot
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
gog_hollow_knight_2.1.0.2.sh
libplayit2.sh
play-hollow-knight.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-hollow-knight.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
