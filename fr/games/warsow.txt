====== War§ow ======

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-warsow.sh|play-warsow.sh]] (mis à jour le 28/12/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 2/04/2018)
  * cible :
    * [[https://www.gog.com/game/warsow|gog_warsow_2.1.0.3.sh]] (MD5 : 028efe7a5f4dfd8851c2146431c7ca4a)
  * dépendances :
    * Arch Linux :
      * libarchive
    * Debian :
      * bsdtar
      * fakeroot

[[https://www.dotslashplay.it/images/games/warsow/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/warsow/thumbnail.jpg?nocache }}]]

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
# pacman -S libarchive
</code>
    - Debian :<code>
# apt-get install bsdtar fakeroot
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
gog_warsow_2.1.0.3.sh
libplayit2.sh
play-warsow.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-warsow.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>

==== Liens ====

  * [[https://fr.wikipedia.org/wiki/Warsow|article Wikipédia]]
  * [[https://www.warsow.net/|site officiel]] (en anglais)
