====== A Boy and His Blob ======

===== Description =====

[[https://www.dotslashplay.it/images/games/a-boy-and-his-blob/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/a-boy-and-his-blob/thumbnail.jpg?nocache }}]]

**A Boy and His Blob** : le sort du pays de Blobolonia, menacé par un infâme empereur, repose entièrement sur les frêles épaules d'un petit garçon.\\
Avec une bonne réserve de bonbons, une pointe de travail d’équipe et un meilleur ami du nom de blob, vous allez l’aider à progresser à travers les niveaux originaux et impressionnants.

  * age : +7 ans
  * catégorie : plate-forme, réflexion
  * date de sortie : 2009

===== Informations =====

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-a-boy-and-his-blob.sh|play-a-boy-and-his-blob.sh]] (mis à jour le 30/01/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 2/04/2018)
  * cibles :
    * [[https://www.gog.com/game/a_boy_and_his_blob|gog_a_boy_and_his_blob_2.1.0.2.sh]] (MD5 : 7025963a3a26f838877374f72ce3760d)
    * [[https://www.dotslashplay.it/ressources/a-boy-and-his-blob/|a-boy-and-his-blob_icons.tar.gz]] (MD5 : 2a555c1f6b02a45b8932c8e72a9c1dd6)
  * dépendances :
    * Arch Linux :
      * libarchive
    * Debian :
      * bsdtar
      * fakeroot

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
# pacman -S libarchive
</code>
    - Debian :<code>
# apt-get install bsdtar fakeroot
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
a-boy-and-his-blob_icons.tar.gz
gog_a_boy_and_his_blob_2.1.0.2.sh
libplayit2.sh
play-a-boy-and-his-blob.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-a-boy-and-his-blob.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
