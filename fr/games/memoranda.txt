====== Memoranda ======

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-memoranda.sh|play-memoranda.sh]] (mis à jour le 3/03/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 2/04/2018)
  * cibles :
    * [[https://www.gog.com/game/memoranda|gog_memoranda_2.2.0.3.sh]] (MD5 : 9671ebb592d4b4a028fd80f76e96c1a1)
    * [[https://www.dotslashplay.it/ressources/libssl/|libssl_1.0.0_32-bit.tar.gz]] (MD5 : 9443cad4a640b2512920495eaf7582c4)
  * dépendances :
    * Arch Linux :
      * imagemagick
      * libarchive
    * Debian :
      * bsdtar
      * fakeroot
      * imagemagick

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
# pacman -S imagemagick libarchive
</code>
    - Debian :<code>
# apt-get install bsdtar fakeroot imagemagick
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
gog_memoranda_2.2.0.3.sh
libplayit2.sh
libssl_1.0.0_32-bit.tar.gz
play-memoranda.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-memoranda.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
