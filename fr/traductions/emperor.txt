====== Empereur: L’Empire du Milieu ======

[[https://www.dotslashplay.it/traductions/emperor/|index des archives de traduction]]

Vous trouverez sur la page donnée en lien quatre archives de traduction :
  * emperorfr-txt.7z (< 1 Mio) traduit les textes
  * emperorfr-snd.7z (~110 Mio) traduit les voix
  * emperorfr-vid.7z (~70 Mio) traduit les vidéos
  * emperorfr-doc.7z (~2 Mio) traduit la documentation

Pour une traduction complète du jeu vous devrez télécharger les trois premières archives.

Une fois les archives qui vous intéressent téléchargées, il vous suffit de les décompresser à la racine du répertoire d’installation du jeu en remplaçant les fichiers déjà en place pour passer votre jeu en français.
