====== Dungeon Keeper 2 ======

[[https://www.dotslashplay.it/traductions/dungeon-keeper-2/|index des archives de traduction]]

Vous trouverez sur la page donnée en lien deux archives de traduction :
  * dk2fr-textes.7z (~240 Kio) traduit les textes
  * dk2fr-voix.7z (~63 Mio) traduit les voix

Pour une traduction complète du jeu vous devrez donc télécharger les deux archives.

Une fois l’archive téléchargée, il vous suffit de la décompresser à la racine du répertoire d’installation du jeu en remplaçant les fichiers déjà en place pour passer votre jeu en français.
